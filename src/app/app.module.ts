import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DangKyComponent } from './Dangnhap-dangky/DangKy/DangKy.component';
import { DangnhapComponent } from './Dangnhap-dangky/dangnhap/dangnhap.component';
import { AdminHomeComponent } from './QuanLy/admin-home/admin-home.component';
import { DashboardComponent } from './QuanLy/Dashboard/Dashboard.component';
import { QuanlyphongComponent } from './QuanLy/Phong/quanlyphong/quanlyphong.component';
import { RouterModule } from '@angular/router';
import { ThemSuaPhongComponent } from './QuanLy/Phong/ThemSuaPhong/ThemSuaPhong.component';
import { QuanLyThuePhongComponent } from './QuanLy/ThuePhong/QuanLyThuePhong/QuanLyThuePhong.component';
import { ChiTietThuePhongComponent } from './QuanLy/ThuePhong/ChiTietThuePhong/ChiTietThuePhong.component';
import { ThemSuaThuePhongComponent } from './QuanLy/ThuePhong/ThemSuaThuePhong/ThemSuaThuePhong.component';
import { QuanlyThongBaoComponent } from './QuanLy/ThongBao/QuanlyThongBao/QuanlyThongBao.component';
import { ThemSuaThongBaoComponent } from './QuanLy/ThongBao/ThemSuaThongBao/ThemSuaThongBao.component';
import { QuanLyKhachHangComponent } from './QuanLy/KhachHang/QuanLyKhachHang/QuanLyKhachHang.component';
import { ThongTinChiTietComponent } from './QuanLy/KhachHang/ThongTinChiTiet/ThongTinChiTiet.component';
import { QuanLyChotDienNuocComponent } from './QuanLy/ChotDienNuoc/QuanLyChotDienNuoc/QuanLyChotDienNuoc.component';
import { MenuComponent } from './KhachHang/menu/menu/menu.component';
import { TrangchuComponent } from './KhachHang/trangchu/Trangchu/Trangchu.component';
import { ChitietTPComponent } from './KhachHang/chi-tiet-thue-phong/chitietTP/chitietTP.component';
import { ChitietchotComponent } from './QuanLy/ChotDienNuoc/Chitietchot/Chitietchot.component';
import { ChiTietThongBaoComponent } from './KhachHang/chi-tiet-thong-bao/chi-tiet-thong-bao/chi-tiet-thong-bao.component';
import { DanhsachthongbaoComponent } from './KhachHang/ThongBao/danhsachthongbao/danhsachthongbao.component';
import { GuiLienHeComponent } from './KhachHang/LienHe/GuiLienHe/GuiLienHe.component';
import { QuanlilienheComponent } from './QuanLy/LienHe/quanlilienhe/quanlilienhe.component';


@NgModule({
  declarations: [
    AppComponent,
    DangnhapComponent,
    DangKyComponent,
    DashboardComponent,
    AdminHomeComponent,
    QuanlyphongComponent,
    ThemSuaPhongComponent,
    QuanLyThuePhongComponent,
    ChiTietThuePhongComponent,
    ThemSuaThuePhongComponent,
    QuanlyThongBaoComponent,
    QuanLyKhachHangComponent,
    ThongTinChiTietComponent,
    QuanLyChotDienNuocComponent,
    ChitietchotComponent,
    ThemSuaThongBaoComponent,
    QuanlilienheComponent,
    // Phần giao diện khách hàng
    MenuComponent,
    TrangchuComponent,
    ChitietTPComponent,
    ChiTietThongBaoComponent,
    DanhsachthongbaoComponent,
    GuiLienHeComponent,
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
