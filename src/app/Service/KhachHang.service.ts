import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { ResponseCode } from '../Enums/ResponseCode.enum';
import { ResponseModel } from '../Model/ResponseModel';
import { User } from '../Model/User';

@Injectable({
  providedIn: 'root'
})
export class KhachHangService {

    public readonly URL ="https://localhost:44374/api/";
    public readonly URLimg ="https://localhost:44374/photos/"

constructor(private http:HttpClient) { }

// Post
chotdiennuocPost(data:any){
    let UserInf = JSON.parse(localStorage.getItem('token')||'{}');
    const header = new HttpHeaders({
    Authorization: `Bearer  ${UserInf.token} `,
  });
  return this.http.post(this.URL+"ChotDienNuoc/guichot",data,{headers:header})
}
 
PostImg(data:any){
    return this.http.post(this.URL+'ChotDienNuoc/SaveFile',data);
  }

danhsachthongbao(){
  let UserInf = JSON.parse(localStorage.getItem('token')||'{}');
  const header = new HttpHeaders({
  Authorization: `Bearer  ${UserInf.token} `,
});
return this.http.get(this.URL+"ThongBao/gioihanThongBao",{headers:header});
}
listthongbao(){
  let UserInf = JSON.parse(localStorage.getItem('token')||'{}');
  const header = new HttpHeaders({
  Authorization: `Bearer  ${UserInf.token} `,
});
return this.http.get(this.URL+'ThongBao/DanhsachThongBao',{headers:header})
}
// Chi tiết thuê phòng của khách
chitietthuephongcuakhach(userID:string){
  let UserInf = JSON.parse(localStorage.getItem('token')||'{}');
  const header = new HttpHeaders({
  Authorization: `Bearer  ${UserInf.token} `,
});
return this.http.get(this.URL+`ThuePhong/thuebyuser/${userID}`,{headers:header});
}
//  Chi tiết thông báo

chitietthongbao(id_ThongBao:number){
  let UserInf = JSON.parse(localStorage.getItem('token')||'{}');
  const header = new HttpHeaders({
  Authorization: `Bearer  ${UserInf.token} `,
});
return this.http.get(this.URL+`ThongBao/Danhsachthongbao/${id_ThongBao}`,{headers:header});
}

// Gửi Liên Hệ
guilienhe(data:any){
  let UserInf = JSON.parse(localStorage.getItem('token')||'{}');
  const header = new HttpHeaders({
  Authorization: `Bearer  ${UserInf.token} `,
});
return this.http.post(this.URL+'LienHe/themLienHe',data,{headers:header});
}

ListTTThanhToan():Observable<any[]>{
  return this.http.get<any>(this.URL+'TinhTrangThanhToan/ListTinhTrang');
}
DanhsachPhong():Observable<any[]>{
  let UserInf = JSON.parse(localStorage.getItem('token')||'{}');
  const header = new HttpHeaders({
    Authorization: `Bearer  ${UserInf.token} `,
  });
  return this.http.get<any>(this.URL+'PhongTro/Danhsachphong',{headers:header});
}
getUser() {
  let UserInf = JSON.parse(localStorage.getItem('token')||'{}');
  const header = new HttpHeaders({
    Authorization: `Bearer  ${UserInf.token} `,
  });
  return this.http
    .get<ResponseModel>(this.URL + 'User/GetUserList',{headers:header})
    .pipe(
      map((res) => {
        let ListUser = Array<User>();
        if (res.responseCode == ResponseCode.Ok) {
          if (res.dateSet) {
            res.dateSet.map((x: User) => {
              ListUser.push(
                new User(x.id,x.hovaten, x.email, x.soDT, x.ngayDangKy,x.diachi,x.roles)
              );
            });
          }
        }
        return ListUser;
      })
    );
}
}



