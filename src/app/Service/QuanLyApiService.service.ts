import { Injectable } from '@angular/core';
import { HttpClient, HttpHandler, HttpHeaders } from '@angular/common/http';
import { ResponseModel } from '../Model/ResponseModel';
import { map, Observable } from 'rxjs';
import { Role } from '../Model/Role';
import { ResponseCode } from '../Enums/ResponseCode.enum';
import { User } from '../Model/User';
import { data, holdReady } from 'jquery';

@Injectable({
  providedIn: 'root'
})
export class QuanLyApiServiceService {

  public readonly URL ="https://localhost:44374/api/";
  public readonly URLimg ="https://localhost:44374/photos/"
constructor(private http:HttpClient) { }



public DangNhap(email: string, password: string) {
  const body = {
    Email: email,
    Password: password,
  };
  return this.http.post<ResponseModel>(this.URL + 'User/Login', body);
}
public DangKy(
  hovaten: string,
  email: string,
  password: string,
  soDT:number,
  diaChi:string,
  roles: string[]
) {
  const body = {
    hovaten: hovaten,
    email: email,
    password: password,
    soDT:soDT,
    diaChi:diaChi,
    roles: roles,
  };
  return this.http.post<ResponseModel>(this.URL + 'User/RegisterUser', body);
}
getUser() {
  let UserInf = JSON.parse(localStorage.getItem('token')||'{}');
  const header = new HttpHeaders({
    Authorization: `Bearer  ${UserInf.token} `,
  });
  return this.http
    .get<ResponseModel>(this.URL + 'User/GetUserList',{headers:header})
    .pipe(
      map((res) => {
        let ListUser = Array<User>();
        if (res.responseCode == ResponseCode.Ok) {
          if (res.dateSet) {
            res.dateSet.map((x: User) => {
              ListUser.push(
                new User(x.id,x.hovaten, x.email, x.soDT, x.ngayDangKy,x.diachi,x.roles)
              );
            });
          }
        }
        return ListUser;
      })
    );
}
getallRole() {
  // let UserInf = JSON.parse(localStorage.getItem('token'));
  // const header = new HttpHeaders({
  //   'Authorization':`Bearer ${UserInf.token} `
  // });
  return this.http.get<ResponseModel>(this.URL + 'User/GetRoles').pipe(
    map((res) => {
      let ListRole = Array<Role>();
      if (res.responseCode == ResponseCode.Ok) {
        if (res.dateSet) {
          res.dateSet.map((x: string) => {
            ListRole.push(new Role(x));
          });
        }
      }
      return ListRole;
    })
  );
}



DanhsachPhong():Observable<any[]>{
  let UserInf = JSON.parse(localStorage.getItem('token')||'{}');
  const header = new HttpHeaders({
    Authorization: `Bearer  ${UserInf.token} `,
  });
  return this.http.get<any>(this.URL+'PhongTro/Danhsachphong',{headers:header});
}
ThemPhong(data:any){
  let UserInf = JSON.parse(localStorage.getItem('token')||'{}');
  const header = new HttpHeaders({
    Authorization: `Bearer  ${UserInf.token} `,
  });
  return this.http.post(this.URL+'PhongTro/themphong',data,{headers:header});
}
SuaPhong(id_Phong:number|string ,data:any){
  let UserInf = JSON.parse(localStorage.getItem('token')||'{}');
  const header = new HttpHeaders({
    Authorization: `Bearer  ${UserInf.token} `,
  });
return this.http.put(this.URL+`PhongTro/suaphong/${id_Phong}`,data,{headers:header});
}
XoaPhong(id_Phong:number|string){
  let UserInf = JSON.parse(localStorage.getItem('token')||'{}');
  const header = new HttpHeaders({
    Authorization: `Bearer  ${UserInf.token} `,
  });
 return this.http.delete(this.URL+`PhongTro/XoaPhong/${id_Phong}`,{headers:header});
}



ListTinhTrang():Observable<any[]>{
  return this.http.get<any>(this.URL+'TinhTrang/ListTinhTrang')
}
ListNhaTro():Observable<any[]>{
  return this.http.get<any>(this.URL+'NhaTro/DanhsachNha');
}
ListTTThanhToan():Observable<any[]>{
  return this.http.get<any>(this.URL+'TinhTrangThanhToan/ListTinhTrang');
}



// Thuê Phòng
Danhsachthuephong():Observable<any[]>{
  let UserInf = JSON.parse(localStorage.getItem('token')||'');
  const header = new HttpHeaders({
    Authorization: `Bearer  ${UserInf.token} `,
  });
  return this.http.get<any>(this.URL+'ThuePhong/DanhsachThue',{headers:header});
}
DangsachthuebyId(id:number){
  let UserInf = JSON.parse(localStorage.getItem('token')||'{}');
  const header = new HttpHeaders({
    Authorization: `Bearer  ${UserInf.token} `,
  });
  return this.http.get(this.URL+'ThuePhong/Danhsachthue/'+id,{headers:header})
}
ThemDanhSachThuePhong(data:any){
  let UserInf = JSON.parse(localStorage.getItem('token')||'{}');
  const header = new HttpHeaders({
    Authorization: `Bearer  ${UserInf.token} `,
  });
 return this.http.post(this.URL+'ThuePhong/themthuephong',data,{headers:header});
}
SuaDanhSachThuePhong(id:number,Data:any){
  let UserInf = JSON.parse(localStorage.getItem('token')||'{}');
  const header = new HttpHeaders({
    Authorization: `Bearer  ${UserInf.token} `,
  });
return this.http.put(this.URL+`ThuePhong/suathuephong/${id}`,Data,{headers:header});
}
XoaThuePhong(id:number){
  let UserInf = JSON.parse(localStorage.getItem('token')||'{}');
  const header = new HttpHeaders({
    Authorization: `Bearer  ${UserInf.token} `,
  });
  return this.http.delete(this.URL+`ThuePhong/XoaThuePhong/${id}`,{headers:header});
}
// Thông Báo

DanhsachThongBao():Observable<any[]>{
  let UserInf = JSON.parse(localStorage.getItem('token')||'');
  const header = new HttpHeaders({
    Authorization: `Bearer  ${UserInf.token} `,
  });
return this.http.get<any>(this.URL+'ThongBao/DanhsachThongBao',{headers:header});
}
ThemThongBao(data:any){
  let UserInf = JSON.parse(localStorage.getItem('token')||'');
  const header = new HttpHeaders({
    Authorization: `Bearer  ${UserInf.token} `,
  });
  return this.http.post(this.URL+'ThongBao/themthongbao',data,{headers:header});
}
SuaThongBao(id_ThongBao:number,data:any){
  let UserInf = JSON.parse(localStorage.getItem('token')||'{}');
  const header = new HttpHeaders({
    Authorization: `Bearer  ${UserInf.token} `,
  });
  return this.http.put(this.URL+`ThongBao/suathongbao/${id_ThongBao}`,data,{headers:header})
}
XoaThongBao(id_ThongBao:number){
  let UserInf = JSON.parse(localStorage.getItem('token')||'');
  const header = new HttpHeaders({
    Authorization: `Bearer  ${UserInf.token} `,
  });
  return this.http.delete(this.URL+`ThongBao/XoaThongBao/${id_ThongBao}`,{headers:header})
}

// Khách Hàng
thongtinthuephongbyUser(userID:string){
  let UserInf = JSON.parse(localStorage.getItem('token')||'');
  const header = new HttpHeaders({
    Authorization: `Bearer  ${UserInf.token} `,
  });
  return this.http.get(this.URL+`ThuePhong/Danhsachthuebyuser/${userID}`,{headers:header})
}
// Khácsh hàng chưa thanh toán

danhsachkhachchuathanhtoan(){
  let UserInf = JSON.parse(localStorage.getItem('token')||'');
  const header = new HttpHeaders({
    Authorization: `Bearer  ${UserInf.token} `,
  });
  return this.http.get(this.URL+'ThuePhong/Danhsachthuechuadongtien',{headers:header})
}

// Phòng trống

DSphongtrong(){
  let UserInf = JSON.parse(localStorage.getItem('token')||'');
  const header = new HttpHeaders({
    Authorization: `Bearer  ${UserInf.token} `,
  });
  return this.http.get(this.URL+'PhongTro/Danhsachphongchuathue',{headers:header})
}

//  Chốt điện nước
danhsachchot(){
  let UserInf = JSON.parse(localStorage.getItem('token')||'');
  const header = new HttpHeaders({
    Authorization: `Bearer  ${UserInf.token} `,
  });
  return this.http.get(this.URL+'ChotDienNuoc/Danhsachchot',{headers:header})
}
chitetchot(id_ChotDN:number){
  let UserInf = JSON.parse(localStorage.getItem('token')||'');
  const header = new HttpHeaders({
    Authorization: `Bearer  ${UserInf.token} `,
  });
  return this.http.get(this.URL+`ChotDienNuoc/Danhsachchotby/${id_ChotDN}`,{headers:header})
}
xoachotdienduoc(id_ChotDN:number){
  let UserInf = JSON.parse(localStorage.getItem('token')||'');
  const header = new HttpHeaders({
    Authorization: `Bearer  ${UserInf.token} `,
  });
  return this.http.delete(this.URL+`ChotDienNuoc/Xoachot/${id_ChotDN}`,{headers:header});
}

//  Liên hệ
Danhsachlienhe(){
  let UserInf = JSON.parse(localStorage.getItem('token')||'');
  const header = new HttpHeaders({
    Authorization: `Bearer  ${UserInf.token} `,
  });
  return this.http.get(this.URL+"LienHe/DanhsachLienHe",{headers:header});
}
XoaLienhe(id_LienHe:number){
  let UserInf = JSON.parse(localStorage.getItem('token')||'');
  const header = new HttpHeaders({
    Authorization: `Bearer  ${UserInf.token} `,
  });
  return this.http.delete(this.URL+`LienHe/XoaLienHe/${id_LienHe}`,{headers:header});
}
}
