export class Role {
    public roles:string = ""
    public isselected:boolean=false

    constructor(roles:string , isselected:boolean = false){
        this.roles = roles,
        this.isselected = isselected
    }
}
