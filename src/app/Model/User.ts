export class User {
    public id:string=""
    public hovaten:string=""
    public email:string=""
    public soDT:number=0
    public ngayDangKy:string=""
    public diachi:string=""
    public roles:string = ""



 constructor(Id:string,Hovaten:string,Email:string,SoDT:number,NgayDangKy:string,DiaChi:string , roles:string){
    this.id=Id;
    this.hovaten=Hovaten;
    this.email=Email;
    this.soDT=SoDT;
    this.ngayDangKy=NgayDangKy;
    this.diachi=DiaChi;
    this.roles=roles;
 }

}
