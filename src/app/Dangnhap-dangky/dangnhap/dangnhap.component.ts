import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ResponseModel } from 'src/app/Model/ResponseModel';
import { User } from 'src/app/Model/User';
import { QuanLyApiServiceService } from 'src/app/Service/QuanLyApiService.service';

@Component({
  selector: 'app-dangnhap',
  templateUrl: './dangnhap.component.html',
  styleUrls: ['./dangnhap.component.css']
})
export class DangnhapComponent implements OnInit {
  FormLogin !: FormGroup;
  constructor(private formbuider: FormBuilder, private api: QuanLyApiServiceService, private Router: Router) { }

  ngOnInit() {
    this.FormLogin = this.formbuider.group(
      {
        email: ["", [Validators.required, Validators.email]],
        password: ["", Validators.required]
      }
    );
  }

  LoginForm() {

    let email = this.FormLogin.controls["email"].value;
    let password = this.FormLogin.controls["password"].value;
    this.api.DangNhap(email, password).subscribe((data: ResponseModel) => {

      if (data.responseCode == 1) {

        localStorage.setItem("token", JSON.stringify(data.dateSet));
        let user =  data.dateSet as User
        if(user.roles == "Admin"){
          this.Router.navigate(["/dashboard"]);
          
        }
       else{
        
         this.Router.navigate(["phong-tro-lai-xa"]);
        
       }
      }
     
      console.log("response", data);
    }, error => {
      
      console.log("error", error)
    })
  }
  onclick(){
    this.Router.navigateByUrl("dang-ky")
  }
}
