import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ResponseCode } from 'src/app/Enums/ResponseCode.enum';
import { ResponseModel } from 'src/app/Model/ResponseModel';
import { Role } from 'src/app/Model/Role';
import { QuanLyApiServiceService } from 'src/app/Service/QuanLyApiService.service';

@Component({
  selector: 'app-DangKy',
  templateUrl: './DangKy.component.html',
  styleUrls: ['./DangKy.component.css']
})
export class DangKyComponent implements OnInit {
  public roles:Role[]=[];
  public registerForm:any;
  constructor(private router:Router , private Api:QuanLyApiServiceService,private formbuider:FormBuilder) { }

  ngOnInit() {
    this.registerForm=this.formbuider.group({
      hovaten:['',[Validators.required]],
      email:['',[Validators.email,Validators.required]],
      password:['',Validators.required],
      soDT:['',Validators.required],
      diaChi:['',Validators.required],
    });
    this.getAllRoles();
  }

  onSubmit(){
    let hovaten=this.registerForm.controls["hovaten"].value;
    let email=this.registerForm.controls["email"].value;
    let password=this.registerForm.controls["password"].value;
    let soDT=this.registerForm.controls["soDT"].value;
    let diaChi=this.registerForm.controls["diaChi"].value;
    this.Api.DangKy(hovaten,email,password,soDT,diaChi,this.roles.filter(x=>x.isselected).map(x=>x.roles)).subscribe((data:ResponseModel)=>{
       if(data.responseCode==ResponseCode.Ok)
       {
        this.registerForm.controls["hovaten"].setValue("");
        this.registerForm.controls["email"].setValue("");
        this.registerForm.controls["password"].setValue("");
        this.registerForm.controls["soDT"].setValue("");
        this.registerForm.controls["diaChi"].setValue("");
        this.roles.forEach(x=>x.isselected=false);
        alert("đăng ký thành công")
        this.router.navigate(["dang-nhap"]);

       }else{
         alert(data.dateSet[0]);
       }
     console.log("response",data);
    },error=>{
      console.log("error",error)
     
    })
  }

  getAllRoles()
  {
    this.Api.getallRole().subscribe(roles=>{
     this.roles=roles;
    });
  }
  onRoleChange(role:string)
  {
  this.roles.forEach(x=>{
    if(x.roles==role)
    {
      x.isselected=!x.isselected;
    }
  
  })
  }
  
  get isRoleSelected()
  {
    return this.roles.filter(x=>x.isselected).length>0;
  }
  onclickDangNhap(){
    this.router.navigateByUrl("dang-nhap")
  }
}
