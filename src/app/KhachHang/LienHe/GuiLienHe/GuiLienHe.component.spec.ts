/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { GuiLienHeComponent } from './GuiLienHe.component';

describe('GuiLienHeComponent', () => {
  let component: GuiLienHeComponent;
  let fixture: ComponentFixture<GuiLienHeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuiLienHeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuiLienHeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
