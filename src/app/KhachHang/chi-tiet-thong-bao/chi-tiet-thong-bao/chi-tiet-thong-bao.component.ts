import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { KhachHangService } from 'src/app/Service/KhachHang.service';

@Component({
  selector: 'app-chi-tiet-thong-bao',
  templateUrl: './chi-tiet-thong-bao.component.html',
  styleUrls: ['./chi-tiet-thong-bao.component.css']
})
export class ChiTietThongBaoComponent implements OnInit {
  ChitietThongBao:any=[];
  idThongBao:any;
  constructor(private API:KhachHangService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.idThongBao = this.activatedRoute.snapshot.paramMap.get('idThongBao');
    this.ChitietTB(this.idThongBao);
    
  }
  ChitietTB(id_ThongBao:any){
    this.API.chitietthongbao(id_ThongBao).subscribe(data=>{
      this.ChitietThongBao = data;
    })
  }
}
