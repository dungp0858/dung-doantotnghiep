import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/Model/User';
import { KhachHangService } from 'src/app/Service/KhachHang.service';

@Component({
  selector: 'app-chitietTP',
  templateUrl: './chitietTP.component.html',
  styleUrls: ['./chitietTP.component.css']
})
export class ChitietTPComponent implements OnInit {
  ListThuebyId:any;
  ListTTTT:any=[];
  ListPhong:any=[];
  ListUser:User[]=[];
  user:any;
  TTTTMap:Map<number, string> = new Map();
  PhongMap:Map<number, string> = new Map();
  UserMap:Map<string, string> = new Map();
  constructor(private API:KhachHangService,private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.user = this.activatedRoute.snapshot.paramMap.get('id');
    this.MapPhong();
    this.MapTTTT();
    this.MapUser();
    this.ChitietThuePhong(this.user);
  }

  ChitietThuePhong(userID:any){
    this.API.chitietthuephongcuakhach(userID).subscribe(data=>{
      this.ListThuebyId = data
    })
  }
  MapTTTT(){
    this.API.ListTTThanhToan().subscribe((data) => {
      this.ListTTTT = data;
      for (let i = 0; i < data.length; i++) {
        this.TTTTMap.set(
          this.ListTTTT[i].idTTThanhToan,
          this.ListTTTT[i].trangThai
        );
      }
    });
   }
   MapPhong(){
    this.API.DanhsachPhong().subscribe((data) => {
      this.ListPhong = data;
      for (let i = 0; i < data.length; i++) {
        this.PhongMap.set(
          this.ListPhong[i].id_Phong,
          this.ListPhong[i].tenPhong
        );
      }
    });
   }
   MapUser(){
    this.API.getUser().subscribe((data) => {
      this.ListUser = data;
      for (let i = 0; i < data.length; i++) {
        this.UserMap.set(
          this.ListUser[i].id,
          this.ListUser[i].hovaten
        );
      }
    });
   }

}
