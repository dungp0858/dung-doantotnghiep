import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { KhachHangService } from 'src/app/Service/KhachHang.service';

@Component({
  selector: 'app-Trangchu',
  templateUrl: './Trangchu.component.html',
  styleUrls: ['./Trangchu.component.css']
})
export class TrangchuComponent implements OnInit {
  ListThongBao:any=[];
  User:any;
  @Input()
  id_ChotDN:number=0;
  userID:string="";
  ngayChot:string="";
  imgDien:string="";
  imgNuoc:string="";

  constructor(private API:KhachHangService,private Router:Router) { }

  ngOnInit() {
    this.User = JSON.parse(localStorage.getItem('token')||'{}');
    this.DanhsachTBhome();
  }
//  -----------------------------------------Chốt Điện Nước--------------------------------------------
  AddChot(){
     var chot = {
      userID:this.userID,
      ngayChot:this.ngayChot,
      imgDien:this.imgDien,
      imgNuoc:this.imgNuoc
     };
    this.API.chotdiennuocPost(chot).subscribe(data=>{
      alert("Gửi thành công")
      location.reload();
    })
  }
  uploadImgDien($event:any){
    var file= $event.target.files[0];
    const formData:FormData=new FormData();
    formData.append('uploadedFile',file,file.name);

    this.API.PostImg(formData).subscribe((data:any)=>{
      this.imgDien=data.toString();
    });
  }
  uploadImgNuoc($event:any){
    var file= $event.target.files[0];
    const formData:FormData=new FormData();
    formData.append('uploadedFile',file,file.name);
    this.API.PostImg(formData).subscribe((data:any)=>{
      this.imgNuoc=data.toString();
    });
  }
// -------------------------------------------End chốt điện nước------------------------------------------
// -------------------------------------------List thông báo Home ----------------------------------------
DanhsachTBhome(){
  this.API.danhsachthongbao().subscribe(data=>{
    this.ListThongBao = data;
  })
}
// ------------------------------------------- End List thông báo Home ----------------------------------------
onchitiet(User:any){
this.Router.navigateByUrl("phong-tro-lai-xa/chi-tiet/"+User.id)
}
onchitietTB(item:any){
this.Router.navigateByUrl('phong-tro-lai-xa/chi-tiet-thong-bao/'+item.id_ThongBao);
}
}
