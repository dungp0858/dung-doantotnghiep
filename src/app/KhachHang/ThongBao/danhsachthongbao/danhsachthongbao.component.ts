import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { data } from 'jquery';
import { KhachHangService } from 'src/app/Service/KhachHang.service';

@Component({
  selector: 'app-danhsachthongbao',
  templateUrl: './danhsachthongbao.component.html',
  styleUrls: ['./danhsachthongbao.component.css']
})
export class DanhsachthongbaoComponent implements OnInit {
  listThongBao:any=[];
  constructor(private API:KhachHangService,private Router:Router) { }

  ngOnInit() {
    this.DSthongbao();
  }
  DSthongbao(){
    this.API.danhsachthongbao().subscribe(data=>{
      this.listThongBao = data;
    })
  }
  onclick(item:any){
    this.Router.navigateByUrl('phong-tro-lai-xa/chi-tiet-thong-bao/'+item.id_ThongBao)
  }
}
