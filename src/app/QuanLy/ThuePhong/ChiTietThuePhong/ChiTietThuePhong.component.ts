import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/Model/User';
import { QuanLyApiServiceService } from 'src/app/Service/QuanLyApiService.service';

@Component({
  selector: 'app-ChiTietThuePhong',
  templateUrl: './ChiTietThuePhong.component.html',
  styleUrls: ['./ChiTietThuePhong.component.css']
})
export class ChiTietThuePhongComponent implements OnInit {
  ListThuebyId:any;
  ListTTTT:any=[];
  ListPhong:any=[];
  ListUser:User[]=[];
  TTTTMap:Map<number, string> = new Map();
  PhongMap:Map<number, string> = new Map();
  UserMap:Map<string, string> = new Map();
  id:any;

  Modeltitle:string="";
  activeThemSuaThuePhong:boolean=false;
  DSTHUEPHONG:any;
  constructor( private activatedRoute: ActivatedRoute,private API:QuanLyApiServiceService) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.DanhsachthuephongbyId(this.id);
    this.MapTTTT();
    this.MapPhong();
    this.MapUser();
  }
   DanhsachthuephongbyId(id:any){
    this.API.DangsachthuebyId(id).subscribe(data=>{
      this.ListThuebyId =data;
    });
   }
   MapTTTT(){
    this.API.ListTTThanhToan().subscribe((data) => {
      this.ListTTTT = data;
      for (let i = 0; i < data.length; i++) {
        this.TTTTMap.set(
          this.ListTTTT[i].idTTThanhToan,
          this.ListTTTT[i].trangThai
        );
      }
    });
   }
   MapPhong(){
    this.API.DanhsachPhong().subscribe((data) => {
      this.ListPhong = data;
      for (let i = 0; i < data.length; i++) {
        this.PhongMap.set(
          this.ListPhong[i].id_Phong,
          this.ListPhong[i].tenPhong
        );
      }
    });
   }
   MapUser(){
    this.API.getUser().subscribe((data) => {
      this.ListUser = data;
      for (let i = 0; i < data.length; i++) {
        this.UserMap.set(
          this.ListUser[i].id,
          this.ListUser[i].hovaten
        );
      }
    });
   }
   updateThue(ListThuebyId:any){
    this.DSTHUEPHONG=ListThuebyId;
    this.activeThemSuaThuePhong=true;
    this.Modeltitle="Cập nhật thông tin thuê phòng"
   }
   closeModel(){
    this.activeThemSuaThuePhong=false;
    this.DanhsachthuephongbyId(this.id);
   }
}
