import { Component, Input, OnInit } from '@angular/core';
import { data } from 'jquery';
import { User } from 'src/app/Model/User';
import { QuanLyApiServiceService } from 'src/app/Service/QuanLyApiService.service';

@Component({
  selector: 'app-ThemSuaThuePhong',
  templateUrl: './ThemSuaThuePhong.component.html',
  styleUrls: ['./ThemSuaThuePhong.component.css']
})
export class ThemSuaThuePhongComponent implements OnInit {
   ListTTTT:any=[];
   ListUser:User[]=[];
   ListPhong:any=[];


   @Input()
   DSTHUEPHONG:any
   id:number=0;
   userID:string="";
   id_Phong:number=0;
   ngayThue:string="";
   ngayThu:string="";
   giaPhong:number=0;
   tienDuaTruoc:number=0;
   csdDien:number=0;
   cscDien:number=0;
   kwDien:number=0;
   giaDien:number=0;
   csdNuoc:number=0;
   cscNuoc:number=0;
   giaNuoc:number=0;
   idTTThanhToan:number=0;
   thanhTien:number=0;

  constructor(private API:QuanLyApiServiceService) { }

  ngOnInit() {
    this.id=this.DSTHUEPHONG.id;
    this.userID = this.DSTHUEPHONG.userID;
    this.id_Phong = this.DSTHUEPHONG.id_Phong;
    this.ngayThue = this.DSTHUEPHONG.ngayThue;
    this.ngayThu=this.DSTHUEPHONG.ngayThu;
    this.giaPhong = this.DSTHUEPHONG.giaPhong;
    this.tienDuaTruoc=this.DSTHUEPHONG.tienDuaTruoc;
    this.csdDien=this.DSTHUEPHONG.csdDien;
    this.cscDien=this.DSTHUEPHONG.cscDien;
    this.kwDien = this.DSTHUEPHONG.kwDien;
    this.giaDien= this.DSTHUEPHONG.giaDien;
    this.csdNuoc = this.DSTHUEPHONG.csdNuoc;
    this.cscNuoc=this.DSTHUEPHONG.cscNuoc;
    this.giaNuoc = this.DSTHUEPHONG.giaNuoc;
    this.idTTThanhToan = this.DSTHUEPHONG.idTTThanhToan;
    this.thanhTien = this.DSTHUEPHONG.thanhTien;
    this.DanhsachPhong();
    this.ListTinhTrang();
    this.GetUserList();
  }
  tongdien:number=0;
  tongnuoc:number=0;
  tinhtien(){
   this.tongdien=(this.cscDien - this.csdDien)*this.giaDien;
   this.tongnuoc=(this.cscNuoc-this.csdNuoc)*this.giaNuoc;
   
   this.thanhTien=(this.tongdien + this.tongnuoc + this.giaPhong) -this.tienDuaTruoc;
  }
 
  updateEmp(){
    var DSTHUEPHONG={
      id:this.id,
      userID : this.userID,
      id_Phong : this.id_Phong,
      ngayThue : this.ngayThue,
      ngayThu:this.ngayThu,
      giaPhong : this.giaPhong,
      tienDuaTruoc:this.tienDuaTruoc,
      csdDien:this.csdDien,
      cscDien:this.cscDien,
      kwDien : this.kwDien,
      giaDien: this.giaDien,
      csdNuoc : this.csdNuoc,
      cscNuoc:this.cscNuoc,
      giaNuoc : this.giaNuoc,
      idTTThanhToan : this.idTTThanhToan,
      thanhTien : this.thanhTien,
    };
    var id:number=this.id;
    this.API.SuaDanhSachThuePhong(id,DSTHUEPHONG).subscribe(data=>{
      alert("Cập nhật thành công!!!")
      var btnclose = document.getElementById("add-edit-close");
      if(btnclose){
        btnclose.click();
      }
    })
  
  }
  addEmp(){
    var DSTHUEPHONG={
      userID : this.userID,
      id_Phong : this.id_Phong,
      ngayThue : this.ngayThue,
      ngayThu:this.ngayThu,
      giaPhong : this.giaPhong,
      tienDuaTruoc:this.tienDuaTruoc,
      csdDien:this.csdDien,
      cscDien:this.cscDien,
      kwDien : this.kwDien,
      giaDien: this.giaDien,
      csdNuoc : this.csdNuoc,
      cscNuoc:this.cscNuoc,
      giaNuoc : this.giaNuoc,
      idTTThanhToan : this.idTTThanhToan,
      thanhTien : this.thanhTien,
    };
    this.API.ThemDanhSachThuePhong(DSTHUEPHONG).subscribe(data=>{
      alert("thêm thành công !!!")
      var btnclose = document.getElementById("add-edit-close");
      if(btnclose){
        btnclose.click();
      }
    });

  }

  DanhsachPhong(){
    this.API.DanhsachPhong().subscribe(data=>{
      this.ListPhong = data;
    });
  }
  ListTinhTrang(){
    this.API.ListTTThanhToan().subscribe(data=>{
      this.ListTTTT = data;
    });
  }
  GetUserList(){
    this.API.getUser().subscribe(data=>{
      this.ListUser = data
    });
  }
}
