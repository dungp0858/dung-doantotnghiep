import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/Model/User';
import { QuanLyApiServiceService } from 'src/app/Service/QuanLyApiService.service';

@Component({
  selector: 'app-QuanLyThuePhong',
  templateUrl: './QuanLyThuePhong.component.html',
  styleUrls: ['./QuanLyThuePhong.component.css']
})
export class QuanLyThuePhongComponent implements OnInit {
  ListThuePhong:any=[];
  listphong:any=[];
  listUser:User[]=[];

  PhongMap: Map<number, string> = new Map();
  UserMap: Map<string, string> = new Map();


  Modeltitle:string=""
  DSTHUEPHONG:any;
  activeThemSuaThuePhong:boolean = false;
  constructor(private API : QuanLyApiServiceService,private Router:Router) { }

  ngOnInit() {
    this.DanhsachThue();
    this.refeshPhong();
    this.refeshUser();
  }
  DanhsachThue(){
    this.API.Danhsachthuephong().subscribe(data=>{
      this.ListThuePhong= data;
    })
  }
  refeshPhong(){
    this.API.DanhsachPhong().subscribe(dta=>{
      this.listphong =dta;
      for (let i = 0; i < dta.length; i++) {
        this.PhongMap.set(
          this.listphong[i].id_Phong,
          this.listphong[i].tenPhong,
        );
      }
    })
  }
  refeshUser(){
    this.API.getUser().subscribe(data=>{
      this.listUser =data;
      for (let i = 0; i < data.length; i++) {
        this.UserMap.set(
          this.listUser[i].id,
          this.listUser[i].hovaten,
        );
      }
    })
  }

  onchitiet(item:any){
  this.Router.navigateByUrl("dashboard/chi-tiet-thue-phong/"+item.id)
  }
  ondelete(item:any){
    if(confirm(`bạn có muốn xóa không ?`)){
      this.API.XoaThuePhong(item.id).subscribe(res=>{
        alert("xóa thành công!!!")
        var btnclose = document.getElementById("add-edit-close");
        if(btnclose){
          btnclose.click();
        }
      });
    }
  }
  addthuePhong(){
   this.DSTHUEPHONG ={
    id:0,
    userID:null,
    id_Phong:0,
    ngayThue:null,
    ngayThu:null,
    giaPhong:0,
    tienDuaTruoc:0,
    csdDien:0,
    cscDien:0,
    kwDien:0,
    giaDien:0,
    csdNuoc:0,
    cscNuoc:0,
    giaNuoc:0,
    idTTThanhToan:0,
    thanhTien:0,
   }
   this.Modeltitle = " Thêm Danh Sách Thuê Phòng Tháng Mới"
   this.activeThemSuaThuePhong = true;
  }
  closeModel(){
   this.activeThemSuaThuePhong = false;
   this.DanhsachThue();
  }
}
