import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/Model/User';
import { QuanLyApiServiceService } from 'src/app/Service/QuanLyApiService.service';

@Component({
  selector: 'app-Chitietchot',
  templateUrl: './Chitietchot.component.html',
  styleUrls: ['./Chitietchot.component.css']
})
export class ChitietchotComponent implements OnInit {
  IdChot:any;
  ImgPath:string="";
  ctchot:any=[];
  ListUser:User[]=[];
  UserMap:Map<string, string> = new Map();
  constructor(private activatedRoute: ActivatedRoute,private API:QuanLyApiServiceService) { }

  ngOnInit() {
    this.ImgPath = this.API.URLimg;
    this.IdChot = this.activatedRoute.snapshot.paramMap.get('id');
    this.chitietchotDN(this.IdChot);
    this.MapUser();
  }
 chitietchotDN(id_ChotDN:any){
  this.API.chitetchot(id_ChotDN).subscribe(dt=>{
    this.ctchot = dt;
  })
 }
 MapUser(){
  this.API.getUser().subscribe((data) => {
    this.ListUser = data;
    for (let i = 0; i < data.length; i++) {
      this.UserMap.set(
        this.ListUser[i].id,
        this.ListUser[i].hovaten
      );
    }
  });
 }
}
