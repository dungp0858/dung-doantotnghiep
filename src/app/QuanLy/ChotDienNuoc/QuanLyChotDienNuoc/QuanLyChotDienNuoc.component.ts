import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/Model/User';
import { QuanLyApiServiceService } from 'src/app/Service/QuanLyApiService.service';

@Component({
  selector: 'app-QuanLyChotDienNuoc',
  templateUrl: './QuanLyChotDienNuoc.component.html',
  styleUrls: ['./QuanLyChotDienNuoc.component.css']
})
export class QuanLyChotDienNuocComponent implements OnInit {
   ListChot:any=[];
   listUser:User[]=[];
   UserMap: Map<string, string> = new Map();
   UrlPath:string="";
  constructor(private API:QuanLyApiServiceService,private router:Router) { }

  ngOnInit() {
    this.UrlPath = this.API.URLimg;
    this.Danhsachchot();
    this.refeshUser();
  }
  Danhsachchot(){
    this.API.danhsachchot().subscribe(data=>{
      this.ListChot = data 
    })
  }
  onchitiet(item:any){
  this.router.navigateByUrl('dashboard/chi-tiet-chot/'+item.id_ChotDN);
  }
  refeshUser(){
    this.API.getUser().subscribe(data=>{
      this.listUser =data;
      for (let i = 0; i < data.length; i++) {
        this.UserMap.set(
          this.listUser[i].id,
          this.listUser[i].hovaten,
        );
      }
    })
  }
  xoa(item:any){
    if(confirm(`bạn có muốn xóa không ?`)){
      this.API.xoachotdienduoc(item.id_ChotDN).subscribe(res=>{
        alert("xóa thành công!!!")
        this.Danhsachchot();
      });
    }
  }

}
