import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/Model/User';
import { QuanLyApiServiceService } from 'src/app/Service/QuanLyApiService.service';

@Component({
  selector: 'app-ThongTinChiTiet',
  templateUrl: './ThongTinChiTiet.component.html',
  styleUrls: ['./ThongTinChiTiet.component.css']
})
export class ThongTinChiTietComponent implements OnInit {
   userID:any;
   thuephongbyUser:any=[];
   listphong:any=[];
   listUser:User[]=[];
   PhongMap: Map<number, string> = new Map();
   UserMap: Map<string, string> = new Map();

  constructor(private activatedRoute: ActivatedRoute,private API:QuanLyApiServiceService,private router:Router) { }

  ngOnInit() {
    this.userID = this.activatedRoute.snapshot.paramMap.get('id');
    console.log(this.userID);
    this.danhsachthuebyuser(this.userID);
    this.refeshPhong();
    this.refeshUser();
  }
  danhsachthuebyuser(userid:any){
    this.API.thongtinthuephongbyUser(userid).subscribe(data=>{
      this.thuephongbyUser = data;
    })
  }
  refeshPhong(){
    this.API.DanhsachPhong().subscribe(dta=>{
      this.listphong =dta;
      for (let i = 0; i < dta.length; i++) {
        this.PhongMap.set(
          this.listphong[i].id_Phong,
          this.listphong[i].tenPhong,
        );
      }
    })
  }
  refeshUser(){
    this.API.getUser().subscribe(data=>{
      this.listUser =data;
      for (let i = 0; i < data.length; i++) {
        this.UserMap.set(
          this.listUser[i].id,
          this.listUser[i].hovaten,
        );
      }
    })
  }

  onchitiet(item:any){
    this.router.navigateByUrl('dashboard/chi-tiet-thue-phong/'+item.id);
  }
  ondelete(item:any){
    if(confirm(`bạn có muốn xóa không ?`)){
      this.API.XoaThuePhong(item.id).subscribe(res=>{
        alert("xóa thành công!!!")
        var btnclose = document.getElementById("add-edit-close");
        if(btnclose){
          btnclose.click();
        }
      });
    }
  }
}
