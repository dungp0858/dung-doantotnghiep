import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/Model/User';
import { QuanLyApiServiceService } from 'src/app/Service/QuanLyApiService.service';

@Component({
  selector: 'app-QuanLyKhachHang',
  templateUrl: './QuanLyKhachHang.component.html',
  styleUrls: ['./QuanLyKhachHang.component.css']
})
export class QuanLyKhachHangComponent implements OnInit {
  ListKhachHang:User[]=[];
  constructor(private API:QuanLyApiServiceService,private Router:Router) { }

  ngOnInit() {
    this.DanhSachKhachHang();
  }
 DanhSachKhachHang(){
  this.API.getUser().subscribe(data=>{
    this.ListKhachHang = data;
  })
 }

 onthongtinct(item:any){
  this.Router.navigateByUrl('dashboard/thong-tin-chi-tiet/'+item.id)
 }
}
