import { Component, OnInit } from '@angular/core';
import { QuanLyApiServiceService } from 'src/app/Service/QuanLyApiService.service';

@Component({
  selector: 'app-quanlyphong',
  templateUrl: './quanlyphong.component.html',
  styleUrls: ['./quanlyphong.component.css']
})
export class QuanlyphongComponent implements OnInit {
  ListPhong:any=[];
  ListTT:any=[];
  ListNha:any=[];
  Modeltitle:string=""
  activateAddEditPhongComponent: boolean = false;
  DSPhong:any
  TinhTrangMap: Map<number, string> = new Map();
  NhaTroMap: Map<number, string> = new Map();
  constructor(private Api:QuanLyApiServiceService) { }

  ngOnInit() {
    this.Danhsachphong();
    this.refreshTTMap();
    this.refreshNhaMap();
  }

  Danhsachphong(){
    this.Api.DanhsachPhong().subscribe(data=>{
      this.ListPhong =  data;
    })
  }
  refreshTTMap() {
    this.Api.ListTinhTrang().subscribe((data) => {
      this.ListTT = data;
      for (let i = 0; i < data.length; i++) {
        this.TinhTrangMap.set(
          this.ListTT[i].maTinhTrang,
          this.ListTT[i].trangThai
        );
      }
    });
  }
  refreshNhaMap() {
    this.Api.ListNhaTro().subscribe((data) => {
      this.ListNha = data;
      for (let i = 0; i < data.length; i++) {
        this.NhaTroMap.set(
          this.ListNha[i].id_NhaTro,
          this.ListNha[i].chuNha
        );
      }
    });
  }
  addPhong(){
    this.DSPhong={
      id_Phong:0,
      tenPhong:null,
      maTinhTrang:0,
      id_NhaTro:0,
    
     }
     this.Modeltitle = "Thêm Phòng"
     this. activateAddEditPhongComponent=true
  }
  updatePhong(item:any){
    this.DSPhong = item;
    this.Modeltitle = "Cập nhật phòng";
    this.activateAddEditPhongComponent = true;
  }
  deletePhong(item:any){
    if(confirm(`bạn có muốn xóa không ?`)){
      this.Api.XoaPhong(item.id_Phong).subscribe(res=>{
        alert("xóa thành công!!!")
        var btnclose = document.getElementById("add-edit-close");
        if(btnclose){
          btnclose.click();
        }
      });
    }
  }
  closeModel(){
    this. activateAddEditPhongComponent=false;
    this.Danhsachphong();
  }
}
