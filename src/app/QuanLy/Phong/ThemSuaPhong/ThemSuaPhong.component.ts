import { Component, Input, OnInit } from '@angular/core';
import { data } from 'jquery';
import { QuanLyApiServiceService } from 'src/app/Service/QuanLyApiService.service';

@Component({
  selector: 'app-ThemSuaPhong',
  templateUrl: './ThemSuaPhong.component.html',
  styleUrls: ['./ThemSuaPhong.component.css']
})
export class ThemSuaPhongComponent implements OnInit {
  Listtt:any=[];
  listnhatro:any=[];
 @Input()
 DSPhong:any
 id_Phong:number=0
 tenPhong:string=""
 maTinhTrang:number=0
 id_NhaTro:number=0
  constructor(private API:QuanLyApiServiceService) { }

  ngOnInit() {
    this.id_Phong= this.DSPhong.id_Phong;
    this.tenPhong=this.DSPhong.tenPhong;
    this.maTinhTrang= this.DSPhong.maTinhTrang;
    this.id_NhaTro=this.DSPhong.id_NhaTro;
    this.ListTTrang();
    this.ListNha();
  }

  ListTTrang(){ 
    this.API.ListTinhTrang().subscribe(dta=>{
      this.Listtt = dta;
    })

  }
  ListNha(){
    this.API.ListNhaTro().subscribe(dta=>{
      this.listnhatro = dta;
    })

  }
  updateEmp(){
    var DSPhong = {
      id_Phong:this.id_Phong,
      tenPhong: this.tenPhong,
      maTinhTrang: this.maTinhTrang,
      id_NhaTro: this.id_NhaTro,
    };
    var idphong:number=this.id_Phong;
    this.API.SuaPhong(idphong,DSPhong).subscribe(data=>{
       alert("Cập nhật thành công!!!")
      var btnclose = document.getElementById("add-edit-close");
      if(btnclose){
        btnclose.click();
      }
    });
  }
  addEmp(){
    var DSPhong = {
      tenPhong: this.tenPhong,
      maTinhTrang: this.maTinhTrang,
      id_NhaTro: this.id_NhaTro,
     
    };
    this.API.ThemPhong(DSPhong).subscribe(res=>{
      alert("thêm thành công!!!")
      var btnclose = document.getElementById("add-edit-close");
      if(btnclose){
        btnclose.click();
      }
    });
  }
}
