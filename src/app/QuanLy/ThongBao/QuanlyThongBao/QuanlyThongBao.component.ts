import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { QuanLyApiServiceService } from 'src/app/Service/QuanLyApiService.service';

@Component({
  selector: 'app-QuanlyThongBao',
  templateUrl: './QuanlyThongBao.component.html',
  styleUrls: ['./QuanlyThongBao.component.css']
})
export class QuanlyThongBaoComponent implements OnInit {
  ListThongBao:any=[];


  Modeltitle:string="";
  activeThemsuathongbao:boolean = false;
  ThongBao:any
  constructor(private router:Router,private API:QuanLyApiServiceService) { }

  ngOnInit() {
    this.DanhSachTB();
  }
  DanhSachTB(){
    this.API.DanhsachThongBao().subscribe(data=>{
      this.ListThongBao = data;
    })
  }
  deletethongbao(item:any){
    if(confirm(`bạn có muốn xóa không ?`)){
      this.API.XoaThongBao(item.id_ThongBao).subscribe(res=>{
        alert("xóa thành công!!!")
        var btnclose = document.getElementById("add-edit-close");
        if(btnclose){
          btnclose.click();
        }
      });
    }
  }
  addThongBao(){ 
   this.ThongBao = {
        id_ThongBao:0,
        tieuDe:null,
        ngayDang:null,
        noiDung:null,
       }
   this.Modeltitle="Thêm Thông Báo";
   this.activeThemsuathongbao = true;
  }
  updateThongBao(item:any){
   this.ThongBao = item;
   this.activeThemsuathongbao = true;
   this.Modeltitle ="cập nhật thông báo "
  }
  closeModel(){
   this.activeThemsuathongbao =  false;
   this.DanhSachTB();
  }
}
