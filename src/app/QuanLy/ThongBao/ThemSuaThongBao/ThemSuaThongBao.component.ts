import { Component, Input, OnInit } from '@angular/core';
import { QuanLyApiServiceService } from 'src/app/Service/QuanLyApiService.service';

@Component({
  selector: 'app-ThemSuaThongBao',
  templateUrl: './ThemSuaThongBao.component.html',
  styleUrls: ['./ThemSuaThongBao.component.css']
})
export class ThemSuaThongBaoComponent implements OnInit {

  @Input()
  ThongBao:any
  id_ThongBao:number=0
  tieuDe:string=""
  ngayDang:string=""
  noiDung:string=""
  constructor(private API:QuanLyApiServiceService) { }

  ngOnInit() {
    this.id_ThongBao = this.ThongBao.id_ThongBao
    this.tieuDe=this.ThongBao.tieuDe;
    this.ngayDang=this.ThongBao.ngayDang;
    this.noiDung=this.ThongBao.noiDung;
  }
  addEmp(){
    var ThongBao={
      tieuDe:this.tieuDe,
      ngayDang:this.ngayDang,
      noiDung:this.noiDung,
    }
    this.API.ThemThongBao(ThongBao).subscribe(data=>{
      alert("thêm thành công")
      var btnclose = document.getElementById("add-edit-close");
      if(btnclose){
        btnclose.click();
      }
    })
  }
  updateEmp(){
    var ThongBao={
      id_ThongBao:this.id_ThongBao,
      tieuDe:this.tieuDe,
      ngayDang:this.ngayDang,
      noiDung:this.noiDung,
    }
    var id_ThongBao:number = this.id_ThongBao;
    this.API.SuaThongBao(id_ThongBao,ThongBao).subscribe(data=>
      {
        alert("cập nhật thành công!!!")
        var btnclose = document.getElementById("add-edit-close");
        if(btnclose){
        btnclose.click();
        }
      })
  }
}
