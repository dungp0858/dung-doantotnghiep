import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/Model/User';
import { QuanLyApiServiceService } from 'src/app/Service/QuanLyApiService.service';

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.css']
})
export class AdminHomeComponent implements OnInit {
  listchuathanhtoan:any=[];
  ListTTTT:any=[];
  ListPhong:any=[];
  ListUser:User[]=[];
  
  // Phòng chưa thuê
  ListPhongchuathue:any=[];
  ListTT:any=[];
  ListNha:any=[];

  TTTTMap:Map<number, string> = new Map();
  PhongMap:Map<number, string> = new Map();
  UserMap:Map<string, string> = new Map();

  // Phòng Chưa thuê
  TinhTrangMap: Map<number, string> = new Map();
  NhaTroMap: Map<number, string> = new Map();

  constructor(private API:QuanLyApiServiceService,private Router:Router) { }

  ngOnInit() {
    this.MapTTTT();
    this.MapPhong();
    this.MapUser();
    this.danhsachchuathanhtoan();

    // danh sách phòng trống
    this.refreshTTMap();
    this.refreshNhaMap();
    this.DSphongchuaT();
  }
  danhsachchuathanhtoan(){
    this.API.danhsachkhachchuathanhtoan().subscribe(data=>{
      this.listchuathanhtoan = data;
    })
  }
  MapTTTT(){
    this.API.ListTTThanhToan().subscribe((data) => {
      this.ListTTTT = data;
      for (let i = 0; i < data.length; i++) {
        this.TTTTMap.set(
          this.ListTTTT[i].idTTThanhToan,
          this.ListTTTT[i].trangThai
        );
      }
    });
   }
   MapPhong(){
    this.API.DanhsachPhong().subscribe((data) => {
      this.ListPhong = data;
      for (let i = 0; i < data.length; i++) {
        this.PhongMap.set(
          this.ListPhong[i].id_Phong,
          this.ListPhong[i].tenPhong
        );
      }
    });
   }
   MapUser(){
    this.API.getUser().subscribe((data) => {
      this.ListUser = data;
      for (let i = 0; i < data.length; i++) {
        this.UserMap.set(
          this.ListUser[i].id,
          this.ListUser[i].hovaten
        );
      }
    });
   }

  //  phòng chưa thuê

  DSphongchuaT(){
    this.API.DSphongtrong().subscribe(data=>{
      this.ListPhongchuathue = data;
    })
  }

  refreshTTMap() {
    this.API.ListTinhTrang().subscribe((data) => {
      this.ListTT = data;
      for (let i = 0; i < data.length; i++) {
        this.TinhTrangMap.set(
          this.ListTT[i].maTinhTrang,
          this.ListTT[i].trangThai
        );
      }
    });
  }
  refreshNhaMap() {
    this.API.ListNhaTro().subscribe((data) => {
      this.ListNha = data;
      for (let i = 0; i < data.length; i++) {
        this.NhaTroMap.set(
          this.ListNha[i].id_NhaTro,
          this.ListNha[i].chuNha
        );
      }
    });
  }

  onqlphong(){
  this.Router.navigateByUrl('dashboard/quan-ly-phong')
  }
  onqltPhong(){
    this.Router.navigateByUrl('dashboard/quan-ly-thue-phong')
  }
  onqltbao(){
    this.Router.navigateByUrl('dashboard/quan-ly-thong-bao')
  }
  onqlkh(){
    this.Router.navigateByUrl('dashboard/quan-ly-khach-hang')
  }
  onqlchot(){
    this.Router.navigateByUrl('dashboard/quan-ly-thue-phong')
  }
}
