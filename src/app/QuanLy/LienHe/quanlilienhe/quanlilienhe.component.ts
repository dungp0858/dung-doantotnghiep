import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/Model/User';
import { QuanLyApiServiceService } from 'src/app/Service/QuanLyApiService.service';

@Component({
  selector: 'app-quanlilienhe',
  templateUrl: './quanlilienhe.component.html',
  styleUrls: ['./quanlilienhe.component.css']
})
export class QuanlilienheComponent implements OnInit {
  ListPhong:any=[];
  ListUser:User[]=[];
  ListLienHe:any=[];

  PhongMap:Map<number, string> = new Map();
  UserMap:Map<string, string> = new Map();

  constructor(private API:QuanLyApiServiceService) { }

  ngOnInit() {
    this.MapPhong();
    this.MapUser();
    this.danhsachlienhe();
  }

  danhsachlienhe(){
    this.API.Danhsachlienhe().subscribe(data=>{
      this.ListLienHe =data;
    })
  }
  MapPhong(){
    this.API.DanhsachPhong().subscribe((data) => {
      this.ListPhong = data;
      for (let i = 0; i < data.length; i++) {
        this.PhongMap.set(
          this.ListPhong[i].id_Phong,
          this.ListPhong[i].tenPhong
        );
      }
    });
   }
   MapUser(){
    this.API.getUser().subscribe((data) => {
      this.ListUser = data;
      for (let i = 0; i < data.length; i++) {
        this.UserMap.set(
          this.ListUser[i].id,
          this.ListUser[i].hovaten
        );
      }
    });
   }

   xoaLienHe(item:any){
    if(confirm(`bạn có muốn xóa không ?`)){
      this.API.XoaLienhe(item.id_LienHe).subscribe(res=>{
        alert("xóa thành công!!!")
        this.danhsachlienhe();
      });
    }
   }
}
