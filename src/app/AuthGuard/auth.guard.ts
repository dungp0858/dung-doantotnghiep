import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from '../Model/User';

@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate {
  constructor(private router:Router){}
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):boolean{
      const user = JSON.parse(localStorage.getItem("token")||'{}') as User;
      if(user && user.email){
        return true;
      }
      else{
        alert("ban phai dang nhap truoc")
        this.router.navigate(['dang-nhap']);
        return false;
      }
  }
  
}
