import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './AuthGuard/auth.guard';
import { DangKyComponent } from './Dangnhap-dangky/DangKy/DangKy.component';
import { DangnhapComponent } from './Dangnhap-dangky/dangnhap/dangnhap.component';
import { ChiTietThongBaoComponent } from './KhachHang/chi-tiet-thong-bao/chi-tiet-thong-bao/chi-tiet-thong-bao.component';
import { ChitietTPComponent } from './KhachHang/chi-tiet-thue-phong/chitietTP/chitietTP.component';
import { GuiLienHeComponent } from './KhachHang/LienHe/GuiLienHe/GuiLienHe.component';
import { MenuComponent } from './KhachHang/menu/menu/menu.component';
import { DanhsachthongbaoComponent } from './KhachHang/ThongBao/danhsachthongbao/danhsachthongbao.component';
import { TrangchuComponent } from './KhachHang/trangchu/Trangchu/Trangchu.component';
import { AdminHomeComponent } from './QuanLy/admin-home/admin-home.component';
import { ChitietchotComponent } from './QuanLy/ChotDienNuoc/Chitietchot/Chitietchot.component';
import { QuanLyChotDienNuocComponent } from './QuanLy/ChotDienNuoc/QuanLyChotDienNuoc/QuanLyChotDienNuoc.component';
import { DashboardComponent } from './QuanLy/Dashboard/Dashboard.component';
import { QuanLyKhachHangComponent } from './QuanLy/KhachHang/QuanLyKhachHang/QuanLyKhachHang.component';
import { ThongTinChiTietComponent } from './QuanLy/KhachHang/ThongTinChiTiet/ThongTinChiTiet.component';
import { QuanlilienheComponent } from './QuanLy/LienHe/quanlilienhe/quanlilienhe.component';
import { QuanlyphongComponent } from './QuanLy/Phong/quanlyphong/quanlyphong.component';
import { QuanlyThongBaoComponent } from './QuanLy/ThongBao/QuanlyThongBao/QuanlyThongBao.component';
import { ChiTietThuePhongComponent } from './QuanLy/ThuePhong/ChiTietThuePhong/ChiTietThuePhong.component';
import { QuanLyThuePhongComponent } from './QuanLy/ThuePhong/QuanLyThuePhong/QuanLyThuePhong.component';

const routes: Routes = [
  {path:"",redirectTo:"dang-nhap",pathMatch: 'full'},
  {path:"dang-nhap",component:DangnhapComponent},
  {path:"dang-ky",component:DangKyComponent},
  {path:"dashboard",component:DashboardComponent,canActivate:[AuthGuard],
  children:[
   {path:"",redirectTo:"admin-home",pathMatch:'full'},
   {path:"admin-home",component:AdminHomeComponent,canActivate:[AuthGuard]},
   {path:"quan-ly-phong",component:QuanlyphongComponent,canActivate:[AuthGuard]},
   {path:"quan-ly-thue-phong",component:QuanLyThuePhongComponent,canActivate:[AuthGuard]},
   {path:"chi-tiet-thue-phong/:id",component:ChiTietThuePhongComponent,canActivate:[AuthGuard]},
   {path:"quan-ly-thong-bao",component:QuanlyThongBaoComponent,canActivate:[AuthGuard]},
   {path:"quan-ly-khach-hang",component:QuanLyKhachHangComponent,canActivate:[AuthGuard]},
   {path:"thong-tin-chi-tiet/:id",component:ThongTinChiTietComponent,canActivate:[AuthGuard]},
   {path:"quan-ly-chot-dien-nuoc",component:QuanLyChotDienNuocComponent,canActivate:[AuthGuard]},
   {path:"chi-tiet-chot/:id",component:ChitietchotComponent,canActivate:[AuthGuard]},
   {path:"quan-ly-lien-he",component:QuanlilienheComponent,canActivate:[AuthGuard]}

  ]},
  // Phần khách hàng
  {path:"phong-tro-lai-xa",component:MenuComponent,children:[
    {path:"",redirectTo:"trang-chu",pathMatch:'full'},
    {path:"trang-chu",component:TrangchuComponent},
    {path:"chi-tiet/:id",component:ChitietTPComponent},
    {path:"chi-tiet-thong-bao/:idThongBao",component:ChiTietThongBaoComponent},
    {path:"danh-sach-thong-bao",component:DanhsachthongbaoComponent},
    {path:"lien-he",component:GuiLienHeComponent},
  ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
